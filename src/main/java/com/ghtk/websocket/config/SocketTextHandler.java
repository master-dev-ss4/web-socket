package com.ghtk.websocket.config;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class SocketTextHandler extends TextWebSocketHandler {

  @Override
  public void handleTextMessage(WebSocketSession session, TextMessage message)
      throws InterruptedException, IOException {

    String payload = message.getPayload();

    ObjectMapper mapper = new ObjectMapper();
    JsonNode actualObj = mapper.readTree(payload);

    session.sendMessage(
        new TextMessage("Hi " + actualObj.get("user").textValue() + " how may we help you?"));
  }
}
